
package br.com.senai.bluetoothchat;

public interface Constantes {

    int MENSAGEM_ESTADO_ALTERADO = 1;
    int MENSAGEM_LER = 2;
    int MENSAGEM_ESCREVER = 3;
    int MENSAGEM_NOME_DISPOSITIVO = 4;
    int MENSAGEM_TOAST = 5;

    String NOME_DISPOSITIVO = "nome_dispositivo";
    String TOAST = "toast";

}
