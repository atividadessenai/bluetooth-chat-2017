package br.com.senai.bluetoothchat;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class BluetoothChatFragment extends Fragment {

    private static final String TAG = "BluetoothChatFragment";

    private static final int REQUISICAO_DISPOSITIVO_CONECTAR_SEGURO = 1;
    private static final int REQUISICAO_DISPOSITIVO_CONECTAR_INSEGURO = 2;
    private static final int REQUISICAO_ATIVAR_BLUETOOTH = 3;

    private ListView mConversationView;
    private EditText mOutEditText;
    private Button mBtnEnviar;
    private String mNomeDispositivoConectado = null;

    private ArrayAdapter<String> mConversasAdapter;
    private StringBuffer mOutStringBuffer;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService mChatService = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null) {
            FragmentActivity activity = getActivity();
            Toast.makeText(activity, "Bluetooth não disponivel", Toast.LENGTH_LONG).show();
            activity.finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUISICAO_ATIVAR_BLUETOOTH);
        } else if (mChatService == null) {
            setupChat();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) {
            mChatService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService.ESTADO_NONE) {
                mChatService.start();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_bluetooth_chat, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mConversationView = (ListView) view.findViewById(R.id.in);
        mOutEditText = (EditText) view.findViewById(R.id.edit_text_out);
        mBtnEnviar = (Button) view.findViewById(R.id.button_send);
    }

    private void setupChat() {

        mConversasAdapter = new ArrayAdapter<String>(getActivity(), R.layout.mensagem);

        mConversationView.setAdapter(mConversasAdapter);

        mOutEditText.setOnEditorActionListener(mWriteListener);

        mBtnEnviar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                View view = getView();
                if (null != view) {
                    TextView textView = (TextView) view.findViewById(R.id.edit_text_out);
                    String message = textView.getText().toString();
                    sendMessage(message);
                }
            }
        });

        mChatService = new BluetoothChatService(getActivity(), mHandler);
        mOutStringBuffer = new StringBuffer("");
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    private void sendMessage(String message) {
        if (mChatService.getState() != BluetoothChatService.ESTADO_CONECTADO) {
            Toast.makeText(getActivity(), R.string.nao_conectado, Toast.LENGTH_SHORT).show();
            return;
        }

        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mChatService.write(send);

            mOutStringBuffer.setLength(0);
            mOutEditText.setText(mOutStringBuffer);
        }
    }

    private TextView.OnEditorActionListener mWriteListener
            = new TextView.OnEditorActionListener() {
        public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                String message = view.getText().toString();
                sendMessage(message);
            }
            return true;
        }
    };

    private void setStatus(int resId) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(resId);
    }

    private void setStatus(CharSequence subTitle) {
        FragmentActivity activity = getActivity();
        if (null == activity) {
            return;
        }
        final ActionBar actionBar = activity.getActionBar();
        if (null == actionBar) {
            return;
        }
        actionBar.setSubtitle(subTitle);
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            FragmentActivity activity = getActivity();
            switch (msg.what) {
                case Constantes.MENSAGEM_ESTADO_ALTERADO:
                    switch (msg.arg1) {
                        case BluetoothChatService.ESTADO_CONECTADO:
                            setStatus(getString(R.string.titulo_conectado_a, mNomeDispositivoConectado));
                            mConversasAdapter.clear();
                            break;
                        case BluetoothChatService.ESTADO_CONECTANDO:
                            setStatus(R.string.titulo_conectando);
                            break;
                        case BluetoothChatService.ESTADO_OUVINDO:
                        case BluetoothChatService.ESTADO_NONE:
                            setStatus(R.string.titulo_nao_conectado);
                            break;
                    }
                    break;
                case Constantes.MENSAGEM_ESCREVER:
                    byte[] writeBuf = (byte[]) msg.obj;
                    String writeMessage = new String(writeBuf);
                    mConversasAdapter.add("Eu:  " + writeMessage);
                    break;
                case Constantes.MENSAGEM_LER:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    mConversasAdapter.add(mNomeDispositivoConectado + ":  " + readMessage);
                    break;
                case Constantes.MENSAGEM_NOME_DISPOSITIVO:
                    mNomeDispositivoConectado = msg.getData().getString(Constantes.NOME_DISPOSITIVO);
                    if (null != activity) {
                        Toast.makeText(activity, "Conectado a "
                                + mNomeDispositivoConectado, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constantes.MENSAGEM_TOAST:
                    if (null != activity) {
                        Toast.makeText(activity, msg.getData().getString(Constantes.TOAST),
                                Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUISICAO_DISPOSITIVO_CONECTAR_SEGURO:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUISICAO_DISPOSITIVO_CONECTAR_INSEGURO:
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUISICAO_ATIVAR_BLUETOOTH:
                if (resultCode == Activity.RESULT_OK) {
                    setupChat();
                } else {
                    Toast.makeText(getActivity(), R.string.bt_nao_ativado_saindo,
                            Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        String address = data.getExtras()
                .getString(ListaDispositivosActivity.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        mChatService.connect(device, secure);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.bluetooth_chat, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.secure_connect_scan: {
                Intent serverIntent = new Intent(getActivity(), ListaDispositivosActivity.class);
                startActivityForResult(serverIntent, REQUISICAO_DISPOSITIVO_CONECTAR_SEGURO);
                return true;
            }
            case R.id.insecure_connect_scan: {
                Intent serverIntent = new Intent(getActivity(), ListaDispositivosActivity.class);
                startActivityForResult(serverIntent, REQUISICAO_DISPOSITIVO_CONECTAR_INSEGURO);
                return true;
            }
            case R.id.discoverable: {
                ensureDiscoverable();
                return true;
            }
        }
        return false;
    }

}
