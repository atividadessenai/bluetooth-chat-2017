package br.com.senai.bluetoothchat;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import br.com.senai.activities.BaseActivity;

public class TelaPrincipal extends BaseActivity {

    public static final String TAG = "TelaPrincipal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            BluetoothChatFragment fragment = new BluetoothChatFragment();
            transaction.replace(R.id.sample_content_fragment, fragment);
            transaction.commit();
        }
    }
}
